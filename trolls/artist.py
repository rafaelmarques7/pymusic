from lib import google
from lib import genius


class Artist(object):

    def __init__(self, name):
        self.name = name
        self.info = {
            "name": self.name,
            "albums": {},
        }

    def add_albums_name(self, album_list):
        for album_name in album_list:
            self.info["albums"][album_name] = {
                "album_name": album_name,
                "tracks": {},
            }

    def add_album_tracks(self, album_name, track_list):
        for track_name in track_list:
            self.info["albums"][album_name]["tracks"][track_name] = {
                "name": track_name,
                "youtube_link": "",
            }

    def add_track_link(self, album_name, track_name, link):
        self.info["albums"][album_name]["tracks"][track_name]["youtube_link"] = link

    def get_discography(self):
        self.genius = genius.GeniusScraper(self.name)
        self.google = google.Google()
        # get albums
        self.genius.nav_to(self.genius.artist_url)
        self.genius.click_show_albums()
        albums = self.genius.get_albums()
        self.add_albums_name(albums)
        for album in albums:
            # get tracks
            query_params = "{} {} {}".format(self.name, album, "genius")
            self.google.query(query_params)
            album_link = self.google.get_first_link()
            self.genius.nav_to(album_link)
            tracks = self.genius.get_tracks()
            tracks = [track.replace("Lyrics", "") for track in tracks]
            self.add_album_tracks(album, tracks)
            for track in tracks:
                query_params = "{} {} {} {}".format(self.name, album, track, "youtube")
                self.google.query(query_params)
                youtube_link = self.google.get_first_link()
                self.add_track_link(album, track, youtube_link)
            print("first album complete. got: ")
            print(self.info)
            return
