""" Module for scraping Genius web page content

This will be used to scrape the discography of an artist
"""

import requests
from selenium import webdriver
from lib import utils
import time


class Genius(object):
    def __init__(self):
        self.configs = utils.get_configs()
        self.token = self.configs["genius_token"]

    def get_test(self):
        headers = {
            "Authorization": "Bearer {}".format(self.token)
        }
        url = "https://api.genius.com//songs/3039923"
        res = requests.get(url, headers=headers)
        print("status_code: ", res.status_code)
        data = res.json()
        utils.write_json("genius_test.json", data)


def get_artist_url(name):
    return "https://genius.com/artists/{}".format(name.replace(" ", "-"))


class GeniusScraper(object):
    def __init__(self, artist_name):
        self.artist_name = artist_name
        self.artist_url = get_artist_url(self.artist_name)
        self.driver = webdriver.Chrome()

    def nav_to(self, url, sleep_time=5):
        """ navigates toa given page and waits for a predefined time """
        self.driver.get(url)
        time.sleep(sleep_time)

    def click_show_albums(self, sleep_time=2):
        """ Clicks on 'show all albums' """
        x_path = "/html/body/routable-page/ng-outlet/routable-profile-page/ng-outlet/routed-page/" \
                 "profile-page/div[3]/div[2]/artist-songs-and-albums/album-grid/div[2]"
        self.driver.find_element_by_xpath(x_path).click()
        time.sleep(sleep_time)

    def get_albums(self):
        albums_names = []
        i = 1
        while True:
            x_path = "/html/body/div[7]/div[1]/ng-transclude/scrollable-data/div/transclude-injecting-local-scope[{}]" \
                     "/div/mini-album-card/a/div[2]/div/div[1]".format(i)
            albums_elems = self.driver.find_elements_by_xpath(x_path)
            if len(albums_elems) == 0:
                break
            album_name = albums_elems[0].text
            albums_names.append(album_name)
            i += 1
        return albums_names

    def get_tracks(self):
        tracks = []
        i = 1
        while True:
            x_path = '/html/body/routable-page/ng-outlet/album-page/div[2]/div[1]/div/album-tracklist-row[{}]' \
                     '/div/div[2]/a/h3'.format(i)
            track_elems = self.driver.find_elements_by_xpath(x_path)
            if len(track_elems) == 0:
                break
            tracks.append(track_elems[0].text)
            i += 1
        return tracks
