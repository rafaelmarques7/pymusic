""" Handles the download of the bookmarked musics

"""
from lib import pyMusic
from lib.utils import time_it, get_youtube_links


@time_it
def main_parallel():
    youtube_links = get_youtube_links()
    mh = pyMusic.MusicHandler()
    #mh.get_musics_parallel(youtube_links)
    mh.get_musics_linear(youtube_links)

if __name__ == "__main__":
    main_parallel()
    #main_linear()
