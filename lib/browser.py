""" Defines a MyBrowser class, to wrap Selenium and add functionality.

"""

from selenium import webdriver
from lib.utils import run_safe
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException


class MyBrowser(object):
    """ A class to wrap Selenium and add some functionality """

    def __init__(self):
        """ Instantiates a driver, available at self.driver """
        self.driver = webdriver.Chrome()

    @run_safe
    def nav_to(self, url):
        """ Navigates to a given url. It waits for page to load, but NOT for AJAX or such. """
        self.driver.get(url)

    @run_safe
    def nav_to_and_wait_for_element(self, url, find_by, identifier, time_out=10):
        """ Navigates to given url, and wait for a SPECIFIC element to be loaded. """
        if find_by not in ["ID", "CLASS_NAME", "CSS_SELECTOR", "NAME", "XPATH"]:
            return
        self.driver.get(url)
        specific_element = EC.presence_of_element_located((getattr(By, find_by), identifier))
        WebDriverWait(self.driver, time_out).until(specific_element)

    @run_safe
    def wait_for_elem(self, find_by, identifier, time_out=10):
        """ Waits for an element to be(come) visible, or until time_out is reached.
            Returns true if visible, false if time_out is reached
        """
        # assert find_by if valid
        fb_accept = ["ID", "CLASS_NAME", "CSS_SELECTOR", "NAME", "XPATH"]
        if find_by not in fb_accept:
            assert ValueError(f"find_by has a bad value: {find_by}. acceptable values are: {fb_accept}")
        # verify element
        try:
            specific_element = EC.presence_of_element_located((getattr(By, find_by), identifier))
            WebDriverWait(self.driver, time_out).until(specific_element)
            return True
        except TimeoutException:
            return False

    @run_safe
    def open_tabs(self, num_tabs=1):
        """ Opens a given number of tabs. """
        for _ in range(num_tabs):
            self.driver.execute_script("window.open('about:blank', '')")

    @run_safe
    def tab_switch(self, tab_index):
        """ Switch to a given tab"""
        if len(self.driver.window_handles) < tab_index + 1:
            return
        self.driver.switch_to.window(self.driver.window_handles[tab_index])

    def quit(self):
        self.driver.quit()


if __name__ == "__main__":
    mb = MyBrowser()
    link = "https://www.youtube.com/results?search_query=kendrick+lamar+dna"
    mb.nav_to_and_wait_for_element(link, "XPATH", '//*[@id="video-title"]')



