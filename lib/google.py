from selenium import webdriver
from lib.utils import run_safe, slow_down
from lib.browser import  MyBrowser

class GoogleDriver(object):
    def __init__(self):
        self.driver = webdriver.Chrome()

    def query(self, params):
        url = "https://www.google.pt/search?q={}".format(params.replace(" ", "+"))
        self.nav_to(url)

    @slow_down(rate=1.5)
    def nav_to(self, url):
        """ navigates toa given page and waits for a predefined time """
        self.driver.get(url)

    def get_first_link(self):
        x_path_list = [
            '//*[@id="rso"]/div[1]/div/div/div/h3/a',
            '//*[@id="rso"]/div/div/div[1]/div/div/h3/a'
        ]
        for x_path in x_path_list:
            try:
                link = self.driver.find_element_by_xpath(x_path).get_attribute("href")
                return link
            except:
                continue


'''
class YoutubeDriver(object):
    """ Youtube class for querying data. Instantiates a webdriver to control and scrape """

    def __init__(self):
        """ Create Youtube class object. Instantiate webdriver """
        self.driver = webdriver.Chrome()

    def query(self, params):
        url = "https://www.youtube.com/results?search_query={}".format(params.replace(" ", "+"))
        self.nav_to(url)

    @slow_down(rate=1.5)
    def nav_to(self, url):
        """ navigates toa given page and waits for a predefined time """
        self.driver.get(url)

    @run_safe
    def get_first_link(self):
        """ Returns the first link of the *current* page """
        x_path = '//*[@id="video-title"]'
        elem_list = self.driver.find_elements_by_xpath(x_path)
        return elem_list[0].get_attribute("href")

    def quit(self):
        self.driver.quit()
'''


class YoutubeDriver(MyBrowser):
    """ Wraps the MyBrowser class, to add scraping functionality """

    def __init__(self):
        MyBrowser.__init__(self)

    def query(self, params):
        url = "https://www.youtube.com/results?search_query={}".format(params.replace(" ", "+"))
        # self.nav_to_and_wait_for_element(url, "XPATH", '//*[@id="video-title"]')
        self.nav_to(url)

    @run_safe
    def get_first_link(self):
        """ Returns the first link of the *current* page """
        x_path = '//*[@id="video-title"]'
        elem_list = self.driver.find_elements_by_xpath(x_path)
        return elem_list[0].get_attribute("href")

    @run_safe
    def scrape_links_fast(self, queries_data, num_tabs=10):
        """ Receives queries_data (list of dict with 'album_name', 'music_name', 'query'),
            SIDE EFFECT: the dict is extended with a link for each query
        """
        # query counter
        curr_q_1, curr_q_2 = 0, 0
        # create multiple tabs
        self.open_tabs(num_tabs-1)
        # iterate over all queries
        while curr_q_1 < len(queries_data):                 # there might be a bug here! - crawling more tabs than necessary close to the end of the list
            # open a page in every tab
            for tab_index in range(0, num_tabs):
                self.tab_switch(tab_index)
                self.query(queries_data[curr_q_1]["query"])
                curr_q_1 += 1
            # scrape a hyperlink from every tab
            for tab_index in range(0, num_tabs):
                self.tab_switch(tab_index)
                self.wait_for_elem("XPATH", '//*[@id="video-title"]')
                link = self.get_first_link()
                queries_data[curr_q_2]["music_link"] = link
                # increment counter
                curr_q_2 += 1







