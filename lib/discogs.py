# https://api.discogs.com/database/search?type=artist&q=Lorde&key=upebWtvqPAidZGrXHBAf&secret=sJsyDyEngsSuECTMksIjoCXoWwKbUWiK
# -> artist[0][id]

# https://api.discogs.com/artists/1778977/releases
#-> releases[:][type] -> master - albums
#                     -> type: release - single
# -> resource url

# https://api.discogs.com/masters/1164779
# -> tracklist

from lib import utils
from lib.utils import slow_down
from lib.logger import  getLogger


class DiscoApi(object):
    """ Class to handle the communication with Disocgs music API. """

    def __init__(self):
        self.logger = getLogger("music logger")
        self.logger.info("Creating DiscoApi class object")
        # create API base url
        self.base_url = "https://api.discogs.com"
        # load config vars
        self.configs = utils.get_configs()
        self.api_key = self.configs["api_key"]
        self.api_pwd = self.configs["api_pwd"]

    def get_id(self, artist_name):
        """ Requests an artist information, return the artist ID """
        self.logger.info(f"Getting {artist_name}'s artist ID")
        url_search = f"{self.base_url}/database/search?type=artist" \
                     f"&q={artist_name}&key={self.api_key}&secret={self.api_pwd}"
        (status_code, data) = utils.make_request(url_search)
        return data["results"][0]["id"]

    def get_release_list(self, artist_id):
        """ Requests the Artist music info. Returns a list of resources_urls pointing to singles/albums """
        url_releases = f"{self.base_url}/artists/{artist_id}/releases"
        (status_code, data) = utils.make_request(url_releases)
        return [r["resource_url"] for r in data["releases"]]

    @slow_down(rate=1.1)
    def get_release_info(self, url_release):
        """ Returns (album_name, [music_names])for a Single release """
        # set default, overwrite later if its the case
        album_name = "Singles"
        track_list = []
        # make request
        (status_code, data) = utils.make_request(url_release)
        if not data:
            self.logger.warning("Data is empty!")
            return album_name, track_list
        len_tracklist = len(data["tracklist"])
        # overwrite album name, if its an album
        if len_tracklist > 2:
            album_name = data["title"]
        for track_info in data["tracklist"]:
            track_list.append(track_info["title"])
        return album_name, track_list






