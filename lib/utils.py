import os
import re
import json
import time
import platform
import functools
import requests


def make_request(url):
    """ Makes a get request. Returns (status_code, data) """
    data = {}
    req = requests.get(url)
    if req.status_code == 200:
        data = req.json()
    return req.status_code, data


def get_path_from_rel(rel_path, base_path=None):
    cwd = os.getcwd() if not base_path else base_path
    if isinstance(rel_path, str):
        return os.path.join(cwd, rel_path)
    if isinstance(rel_path, list):
        return os.path.join(cwd, *rel_path)


def get_configs():
    config_path = get_path_from_rel("configs.json")
    return load_json(config_path)


def make_dir_safe(abs_path):
    if not os.path.exists(abs_path):
        os.makedirs(abs_path)


def load_json(f_path):
    with open(f_path, 'rb') as f:
        data = json.load(f)
    return data


def write_json(f_name, data):
    f_path = get_path_from_rel(["docs", f_name])
    with open(f_path, 'w') as f:
        json.dump(data, f)
    return


def load_txt_to_list(f_path):
    with open(f_path, 'r') as f:
        content = f.readlines()
    return [x.strip() for x in content]


def is_in_file(f_path, item):
    content = load_txt_to_list(f_path)
    if item in content:
        return True
    return False


def append_to_file(f_path, item):
    """ Adds an item to the file, together with a newline """
    with open(f_path, 'a+') as f:
        f.write(item + "\n")
    return


def verify_os():
    return platform.system()


def get_bookmarks_path():
    b_path = ""
    configs = get_configs()
    user_name = configs["user_name"]
    os_sys = verify_os()
    if os_sys == "Windows":
        # "C:\Users\Y\AppData\Local\Google\Chrome\User Data\Default\bookmarks"
        b_path = os.path.join("C:\\", "users", user_name, "AppData", "local", "google",
                              "chrome", "user data", "default", "bookmarks")
    if os_sys == "Darwin":
        # /Users/<Your UserName>/Library/Application\ Support/Google/Chrome
        b_path = os.path.join("users", user_name, "library", "application support", "google", "chrome")
    return b_path


def get_bookmarks():
    b_path = get_bookmarks_path()
    b_marks = load_json(b_path)
    return b_marks


def get_all_urls(b_dict, url_list=[]):
    """ Returns all urls from the bookmarks, at any depth """
    for key, item in b_dict.items():
        if isinstance(item, dict):
            get_all_urls(item, url_list)
        elif isinstance(item, list):
            for data in item:
                get_all_urls(data, url_list)
        elif key == "url":
            url_list.append(item)
    return url_list


def get_youtube_links():
    """ Filters the youtube links from the bookmarks """
    b_marks = get_bookmarks()
    url_list = get_all_urls(b_marks)
    return [url for url in url_list if "youtube.com" in url]


def safe_filename(s, max_length=255):
    """Sanitize a string making it safe to use as a filename.

    This function was based off the limitations outlined here:
    https://en.wikipedia.org/wiki/Filename.

    :param str s:
        A string to make safe for use as a file name.
    :param int max_length:
        The maximum filename character length.
    :rtype: str
    :returns:
        A sanitized string.
    """
    # Characters in range 0-31 (0x00-0x1F) are not allowed in ntfs filenames.
    ntfs_chrs = [chr(i) for i in range(0, 31)]
    chrs = [
        '\"', '\#', '\$', '\%', '\'', '\*', '\,', '\.', '\/', '\:', '"',
        '\;', '\<', '\>', '\?', '\\', '\^', '\|', '\~', '\\\\',
        #'|', ':', '?', '<', '>',
    ]
    pattern = '|'.join(ntfs_chrs + chrs)
    regex = re.compile(pattern, re.UNICODE)
    filename = regex.sub('', s)
    # return unicode(filename[:max_length].rsplit(' ', 0)[0])
    return filename[:max_length].rsplit(' ', 0)[0]


def run_safe(func, *args, **kwargs):
    @functools.wraps(func)
    def safe_func(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except Exception as e:
            print("There was an error running {}. Error: \n{}".format(func.__name__, e))
    return safe_func


def time_it(func, *args, **kwargs):
    @functools.wraps(func)
    def time_that(*args, **kwargs):
        t_1 = time.time()
        func(*args, **kwargs)
        print("{} executed in {} seconds".format(func.__name__, str(time.time() - t_1)))
    return time_that


def debug(func):
    """Print the function signature and return value"""
    @functools.wraps(func)
    def wrapper_debug(*args, **kwargs):
        args_repr = [repr(a) for a in args]                      # 1
        kwargs_repr = [f"{k}={v!r}" for k, v in kwargs.items()]  # 2
        signature = ", ".join(args_repr + kwargs_repr)           # 3
        print(f"Calling {func.__name__}({signature})")
        value = func(*args, **kwargs)
        print(f"{func.__name__!r} returned {value!r}")           # 4
        return value
    return wrapper_debug


def slow_down(_func=None, *, rate=1):
    """Sleep given amount of seconds before calling the function"""
    def decorator_slow_down(func):
        @functools.wraps(func)
        def wrapper_slow_down(*args, **kwargs):
            time.sleep(rate)
            return func(*args, **kwargs)
        return wrapper_slow_down

    if _func is None:
        return decorator_slow_down
    else:
        return decorator_slow_down(_func)