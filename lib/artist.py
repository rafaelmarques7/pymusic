from lib import utils
from lib.discogs import DiscoApi
from lib.google import YoutubeDriver
from lib.pyMusic import MusicHandler
from lib.logger import getLogger
from lib.utils import run_safe, slow_down


class Artist(object):
    """ This is a class for organizing the data relative to a music artist. """

    def __init__(self, artist_name):
        """ Create class object based on artist name """
        self.logger = getLogger("music logger")
        self.logger.critical(f"Initializing Artist class object for {artist_name}")
        self.artist_name = artist_name
        self.disco_api = DiscoApi()
        self.info = {
            "name": self.artist_name,
            "albums": {},
        }

    def add_album_names(self, album_list):
        """ Creates empty albums based on a list containing the albums' names """
        for album_name in album_list:
            if album_name in self.info["albums"].keys():
                # album already exists
                return
            self.info["albums"][album_name] = {
                "album_name": album_name,
                "tracks": {},
            }

    def add_tracks_to_album(self, album_name, track_list):
        """ Add track names to a given music album """
        for track_name in track_list:
            self.info["albums"][album_name]["tracks"][track_name] = {
                "name": track_name,
                "youtube_link": "",
            }

    def add_track_link(self, album_name, track_name, link):
        self.info["albums"][album_name]["tracks"][track_name]["youtube_link"] = link

    def get_discography_info(self):
        """ Collects information regarding the entire discography of an artist.
            That is,
            Album names, album tracks, singles, youtube links.
        """
        self.logger.info("Getting discography information.")
        artist_id = self.disco_api.get_id(self.artist_name)
        self.logger.info("Getting all official releases")
        url_list_releases = self.disco_api.get_release_list(artist_id)
        self.logger.info("Getting music names")
        self.get_music_names(url_list_releases)
        self.logger.info("Getting music youtube links")
        self.get_music_links()

    def get_music_names(self, url_list_releases):
        for url in url_list_releases:
            album_name, track_list = self.disco_api.get_release_info(url)
            self.add_album_names([album_name])
            self.add_tracks_to_album(album_name, track_list)


    '''
    def get_music_links(self):
        """ Traverses the current artist info, in order to collect youtube links for every music """
        self.logger.info("Getting music links.")
        yt = YoutubeDriver()
        for album_name, album_data in self.info["albums"].items():
            for music_name, music_data in album_data["tracks"].items():
                query_params = f"{self.artist_name} {music_name}"
                yt.query(query_params)
                music_link = yt.get_first_link()
                self.add_track_link(album_name, music_name, music_link)
        yt.quit()
    '''

    @run_safe
    def get_music_links(self):
        yt = YoutubeDriver()
        queries = self.get_all_queries_names()            # list of dict with info about album, music name and query
        yt.scrape_links_fast(queries)                     # queries is modified, and extended with a music_link
        for item in queries:
            self.add_track_link(item["album_name"], item["music_name"], item["music_link"])
        yt.quit()

    def get_all_queries_names(self):
        """ Creates a list of dictionaries, each with 'album_name', 'music_name' and 'query' (for youtube) """
        queries = []
        for album_name, album_data in self.info["albums"].items():
            for music_name, music_data in album_data["tracks"].items():
                q = {
                    "album_name": album_name,
                    "music_name": music_name,
                    "query": f"{self.artist_name} {music_name}",
                    "music_link": "",
                }
                queries.append(q)
        return queries

    def download_discography(self):
        """ Downloads the entire discography of the artist.
            Organize in a decent folder structure.
         """
        self.logger.warning(f"Starting the download of the discography of {self.artist_name}")
        # create downloader object
        mh = MusicHandler()
        # iterate over albums
        for album_name, album_data in self.info["albums"].items():
            self.logger.info(f"Processing album {album_name}")
            # create folder
            album_path = utils.get_path_from_rel([self.artist_name, album_name], base_path=mh.dir_music)
            utils.make_dir_safe(album_path)
            # overwrite dir_path for music download
            mh.dir_video, mh.dir_audio = album_path, album_path
            # placeholder for links
            url_list = []
            # iterate over songs
            for music_name, music_data in album_data["tracks"].items():
                url_list.append(music_data["youtube_link"])
            # download songs
            mh.get_musics_parallel(url_list)
        self.logger.critical(f"Finished downloading {self.artist_name}'s discography.")


if __name__ == "__main__":
    art = Artist("Kendrick Lamar")
    art.get_discography_info()
    art.download_discography()