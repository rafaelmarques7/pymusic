import unittest
from lib import utils


class TestUtils(unittest.TestCase):
    """ Test the utils library

     Run unit test with: python -m specs.utils_spec -v
     from the top lvl directory

    """

    def test_get_bookmarks(self):
        """ Asserts bookmarks is not empty """
        b_marks = utils.get_bookmarks()
        self.assertTrue(b_marks)

    def test_get_youtube_links(self):
        """ Asserts youtube links is not empty list """
        b_marks = utils.get_bookmarks()
        y_links = utils.get_all_urls(b_marks)
        self.assertTrue(y_links)




if __name__ == '__main__':
    unittest.main()