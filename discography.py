""" Handles the download of the entire discography of an artist

"""
from lib.artist import Artist
from lib.utils import time_it
from lib.pyMusic import MusicHandler

@time_it
def main():
    art = Artist("Jhené Aiko")
    art.get_discography_info()
    art.download_discography()


if __name__ == "__main__":
    main()

